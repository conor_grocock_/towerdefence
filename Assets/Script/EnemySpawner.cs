﻿using UnityEngine;
using System.Collections;



public class EnemySpawner : MonoBehaviour {
	/// <summary>
	/// 	Used to created waves easily.
	/// </summary>
	[System.Serializable]
	public class WaveCompnent {
		public GameObject enemyPrefab;
		public int num;
		[System.NonSerialized]
		public int spawned = 0;
	}

	/// <summary>
	/// 	Array of Enemys. Editable in Editor.
	/// </summary>
	public WaveCompnent[] Enemys;

	/// <summary>
	/// 	how long until next enemy spawns.
	/// </summary>
	public float spawnCountDown = 0.25f;

	/// <summary>
	/// Count down until the next enemy spawns.
	/// </summary>
	float spawnCDremaining = 2f;
	
	// Update is called once per frame
	void Update () {
		spawnCDremaining -= Time.deltaTime;
		if(spawnCDremaining <= 0){
			spawnCDremaining = spawnCountDown;
			bool didSpawn = false;

			foreach(WaveCompnent wc in Enemys){
				if(wc.spawned < wc.num){
					//Spawn IT!!
					wc.spawned++;
					didSpawn = true;
					Instantiate(wc.enemyPrefab, this.transform.position, this.transform.rotation);

					break;
				}
			}

			if(didSpawn == false){
				//Wave over
				//Next Wave?
				if(this.transform.parent.childCount == 1){
					foreach(WaveCompnent wc in Enemys){
						wc.spawned = 0;
						wc.num = wc.num * 5;
						wc.enemyPrefab.GetComponent<Enemy>().Health *= 5; 
					}
				}else{
					this.transform.parent.GetChild(1).gameObject.SetActive(true);
					Destroy(gameObject);
				}
			}
		}
	}
}