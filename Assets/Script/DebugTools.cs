﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugTools : MonoBehaviour {

	/// <summary>
	/// 	Refrence to the DevelopnentPanel GameObject.
	/// </summary>
	public GameObject DevelopmentPanel;

	/// <summary>
	/// 	Global refrence to the level paths.
	/// </summary>
	GameObject PathGO;

	// Use this for initialization
	void Start () {
		//DevelopmentPanel = GameObject.Find("DevelopmentPanel");
		PathGO = GameObject.Find("Path");
	}
	
	// Update is called once per frame
	void Update () {
		if(DevelopmentPanel != null){
			if(Input.GetKeyDown(KeyCode.F12)){
				if(DevelopmentPanel.activeInHierarchy){
					DevelopmentPanel.SetActive(false);
				}else{
					DevelopmentPanel.SetActive(true);
				}
			}
		}else{
			Debug.Log("No Debug panel found");
		}
	}

	/// <summary>
	/// 	Enable or Disable the visibility of AI Nodes
	/// </summary>
	public void ToggleAINodes(){
		if(PathGO != null){
			// toggles the visibility of this gameobject and all it's children
			var renderers = PathGO.GetComponentsInChildren<Renderer>();
			foreach (Renderer r in renderers) {
				r.enabled = !r.enabled;
			}
		}
	}

	/// <summary>
	/// 	Kill all enemys
	/// </summary>
	public void KillAll(){
		Enemy[] Enemys = GameObject.FindObjectsOfType<Enemy>();
		foreach(Enemy e in Enemys){
			e.kill();
		}
	}
}
