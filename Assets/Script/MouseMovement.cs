﻿using UnityEngine;
using System.Collections;

public class MouseMovement : MonoBehaviour {

	public float horizontalSpeed = 2.0F;
	public float verticalSpeed = 2.0F;
	public float ZoomSpeed = 2.0F;

	void Update() {
		float h = horizontalSpeed * Input.GetAxis("Mouse X");
		float v = verticalSpeed * Input.GetAxis("Mouse Y");
		transform.Translate(h, v, 0);

		if(Input.GetKeyDown(KeyCode.LeftAlt))
			this.GetComponent<Camera>().orthographicSize += ZoomSpeed * 3;

		if(Input.GetKeyDown(KeyCode.LeftControl))
			this.GetComponent<Camera>().orthographicSize -= ZoomSpeed * 3;
	}
}
