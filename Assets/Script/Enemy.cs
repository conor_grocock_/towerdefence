﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class Enemy : MonoBehaviour {

	/// <summary>
	/// 	The position of the next pathNode.
	/// </summary>
	Transform target;

	/// <summary>
	/// The index of the path node.
	/// </summary>
	int pathNodeIndex = 0;

	/// <summary>
	/// The speed of the Enemy.
	/// </summary>
	public float Speed  = 5f;

	/// <summary>
	/// The health of the Enemy.
	/// </summary>
	public float Health = 10f;

	/// <summary>
	/// The reward given to the Player after killing the Enemy.
	/// </summary>
	public float Reward;

	Path path;

	CharacterController CC;

	/// <summary>
	/// The index of the path node.
	/// </summary>
	int pathIndex = 0;

	// Use this for initialization
	void Start () {
		this.target = GameObject.Find("Target").transform;
		Seeker seeker = this.gameObject.GetComponent<Seeker>();
		if(seeker == null){
			Destroy(gameObject);
		}else{
			seeker.StartPath( this.transform.position, target.position, PathComplete);
		}

		CC = this.GetComponent<CharacterController>();

		if(Reward == 0){
			Reward = workOutReward();
		}
	}

	void PathComplete(Path p){
		if(!p.error){
			this.path 	   = p;
			this.pathIndex = 0;
		}else{
			Debug.LogError(p.error);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(path == null){
			return;
		}

		if(pathIndex >= path.vectorPath.Count){
			ReachedGoal();
			return;
		}

		Vector3 dir = path.vectorPath[pathIndex] - this.transform.localPosition;

		float distThisFrame = Speed * Time.deltaTime;

		if(dir.magnitude <= distThisFrame){
			pathIndex++;
		}else{
			//Movement Code
			transform.Translate( dir.normalized * distThisFrame, Space.World);

			//Rotation Code
			//TODO: Consider ways to rotate

			Quaternion targetRotation = Quaternion.LookRotation(dir);
			this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);
		}
	}

	/// <summary>
	/// Works the reward for killing the Enemy.
	/// </summary>
	/// <returns>The reward.</returns>
	float workOutReward(){
		return Mathf.CeilToInt((Health * Speed) / Random.Range(10, 100));
	}

	/// <summary>
	/// 	Runs whem Enemy reaches the final AI Node.
	/// </summary>
	void ReachedGoal(){
		GameObject.FindObjectOfType<ScoreManager>().LostLife();
		Destroy(gameObject);
	}

	/// <summary>
	/// Take damage. Able to be called from any Enemy Object
	/// </summary>
	/// <param name="damage">Damage.</param>
	public void TakeDamage(float damage){
		Health -= damage;
		if(Health <= 0){
			die();
		}
	}

	/// <summary>
	/// Kill the enemy.
	/// </summary>
	public void kill(){
		die();
	}

	/// <summary>
	/// 	Adds monetery reward. Destroys GameObject.
	/// </summary>
	void die(){
		GameObject.FindObjectOfType<ScoreManager>().addMoney(Reward);
		Destroy(gameObject);
	}
}
