﻿//C# Example
using UnityEditor;
using UnityEngine;

public class EnemyCreationWindow : EditorWindow
{
	string     Name;
	GameObject Model;
	float      Health;
	float      Speed;
	float      Reward;

	// Add menu item named "My Window" to the Window menu
	[MenuItem("Window/My Window")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(EnemyCreationWindow));
	}

	void OnGUI()
	{
		GUILayout.Label ("Enemy Creation", EditorStyles.boldLabel);
		Name = EditorGUILayout.TextField ("Name", Name);
		Model = (GameObject)EditorGUILayout.ObjectField("Model", Model, typeof(GameObject), false);
		EditorGUILayout.Space();
		Health = EditorGUILayout.FloatField("Health", Health);
		Speed = EditorGUILayout.FloatField("Speed", Speed);
		Reward = EditorGUILayout.FloatField("Reward", Reward);

		bool btn = GUILayout.Button("Create Enemy");

		if(btn){
			Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/" + Name + ".prefab");
			GameObject EnemyGO = new GameObject();
			Enemy e = EnemyGO.gameObject.AddComponent<Enemy>();
			e.Health = Health;
			e.Speed = Speed;
			if(Reward != 0)
				e.Reward = Reward;
			else 
				e.Reward = float.NaN;

			MeshFilter filter = EnemyGO.AddComponent<MeshFilter>();
			filter.sharedMesh = Model.GetComponentInChildren<MeshFilter>().sharedMesh;

			MeshRenderer Renderer = EnemyGO.AddComponent<MeshRenderer>();
			Renderer.sharedMaterial = Model.GetComponentInChildren<MeshRenderer>().sharedMaterial;

			PrefabUtility.ReplacePrefab(EnemyGO, prefab);
			AssetDatabase.Refresh();
		}

	}
}