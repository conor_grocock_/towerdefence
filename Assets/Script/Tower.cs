﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour {

	Transform turretTransform;

	public GameObject bulletPrefab;

	public float Range        = 10f;
	public float fireCooldown = 0.5f;
	public float Cost         = 5f;
	public float Damage       = 1f;
	public float Radius       = 0f;
	public float Bulletspeed  = 15f;

	Enemy nearistEnemy;
	float fireCooldownLeft;

	// Use this for initialization
	void Start () {
		turretTransform = transform.Find("Turret");
		if(turretTransform == null){
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if(nearistEnemy == null){
			nearistEnemy = setTargetbyClosest();
		}else{
			Vector3 dir = nearistEnemy.transform.position - this.transform.position;

			Quaternion lookRot = Quaternion.LookRotation(dir);

			turretTransform.rotation = Quaternion.Euler(0,lookRot.eulerAngles.y ,0);

			fireCooldownLeft -= Time.deltaTime;
			if(fireCooldownLeft <= 0){
				ShootAt(nearistEnemy);
			}
		}

	}

	Enemy setTargetbyClosest(){
		Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

		Enemy nearistEnemy = null;
		float dist = Range;

		foreach (Enemy e in enemies){
			float d = Vector3.Distance(this.transform.position, e.transform.position);
			if(d < dist){
				nearistEnemy = e;
				dist = d;
			}
		}

		if(nearistEnemy == null){
			return null;
		}

		return nearistEnemy;
	}

	void ShootAt(Enemy e){
		fireCooldownLeft = fireCooldown;

		//TODO: Fire out the tip
		GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, this.transform.position, this.transform.rotation);

		Bullet b = bulletGO.GetComponent<Bullet>();
		b.target = e.transform;
		b.damage = Damage;
		b.radius = Radius;
		b.speed  = Bulletspeed;
	}
}
