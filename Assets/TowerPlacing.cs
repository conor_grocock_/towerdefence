﻿using UnityEngine;
using System.Collections;

public class TowerPlacing : MonoBehaviour {

	public float grid_scale = 1f;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonUp(0)){
			var currentPos = Input.mousePosition;

			RaycastHit hit;

			Ray ray = Camera.main.ScreenPointToRay(currentPos);

			if (Physics.Raycast(ray, out hit)) {
				Transform objectHit = hit.transform;

				Debug.Log(objectHit.name);

				if(objectHit.name == "Map"){
					Debug.Log("Creating tower");

					BuildingManager bm = GameObject.FindObjectOfType<BuildingManager>();

					if(bm.TowerPrefab != null){
						ScoreManager sm = GameObject.FindObjectOfType<ScoreManager>();

						if(sm.hasEnoughtMoney(bm.TowerPrefab.GetComponent<Tower>().Cost)){
							
							GameObject tower = (GameObject)Instantiate(bm.TowerPrefab, Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y, 0f)), transform.rotation);

							tower.transform.position = new Vector3(tower.transform.position.x, 0f, tower.transform.position.z);

							sm.takeMoney(bm.TowerPrefab.GetComponent<Tower>().Cost);
						}else{
							Debug.Log("Not enought money");
						}
					}
				}
			}
		}
	}
}
