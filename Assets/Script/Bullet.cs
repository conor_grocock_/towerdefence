﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	/// <summary>
	/// 	The Speed of the bullet.
	/// </summary>
	public float speed = 15f;

	/// <summary>
	/// 	The radius of the explosion.
	/// 	If the radius is zero then no explosion is triggered.
	/// </summary>
	public float radius = 1f;

	/// <summary>
	/// 	The damage that the bullet does to the target it hits.
	/// </summary>
	public float damage = 1f;

	/// <summary>
	/// The Target.
	/// </summary>
	public Transform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// Checks if target is null. Stops pottential errors later.
		if(target == null){
			Destroy(gameObject);
			return;
		}

		Vector3 dir = target.position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if(dir.magnitude <= distThisFrame){
			DoBulletHit();
		}else{
			
			//Movement Code
			transform.Translate( dir.normalized * distThisFrame, Space.World);

			//Rotation Code
			//TODO: Consider ways to rotate

			Quaternion targetRotation = Quaternion.LookRotation(dir);
			this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 5);
		}
	}

	/// <summary>
	/// 	Called when bullet collides with target.
	/// </summary>
	void DoBulletHit(){
		if(radius == 0){
			target.GetComponent<Enemy>().TakeDamage(damage);
		}
		else{
			Collider[] cols = Physics.OverlapSphere(this.transform.position, radius);

			foreach(Collider c in cols){
				Enemy e = c.transform.parent.GetComponent<Enemy>();
				if(e != null){
					e.TakeDamage(damage);
				}
			}
		}
		Destroy(gameObject);
	}
}
