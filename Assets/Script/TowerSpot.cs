﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TowerSpot : MonoBehaviour {

	public Text NotEnoughMoney;

	public float TimetillmessageDisapers = 1f;
	float TimetillmessageDisapersremaining;

	/// <summary>
	/// 	Builds tower if the PLayer has enough money
	/// </summary>
	void OnMouseUp(){
		BuildingManager bm = GameObject.FindObjectOfType<BuildingManager>();
		if(bm.TowerPrefab != null){
			ScoreManager sm = GameObject.FindObjectOfType<ScoreManager>();
			if(sm.hasEnoughtMoney(bm.TowerPrefab.GetComponent<Tower>().Cost)){
				Instantiate(bm.TowerPrefab, transform.position, transform.rotation);
				sm.takeMoney(bm.TowerPrefab.GetComponent<Tower>().Cost);
				Destroy(gameObject);
			}else{
				Debug.Log("Not enought money");
			}
		}
	}
}
