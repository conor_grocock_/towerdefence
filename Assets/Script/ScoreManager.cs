﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

/// <summary>
/// 	Manages the score
/// </summary>
public class ScoreManager : MonoBehaviour {

	/// <summary>
	/// 	Number of lives left.
	/// </summary>
	public float lives = 20;

	/// <summary>
	/// 	The money left.
	/// </summary>
	public float money = 100;

	/// <summary>
	/// 	Text elements that need updating when amount of money changes.
	/// </summary>
	public Text[] Money;

	/// <summary>
	/// 	Text elements that need updating when the lives count changes.
	/// </summary>
	public Text[] Lives;

	/// <summary>
	/// 	Adds the amount to the amount of money. 
	/// </summary>
	/// <param name="amount">Amount off money to add.</param>
	public void addMoney(float amount){
		money += amount;
		updateUI();
	}

	/// <summary>
	/// 	Takes the amount to the amount of money. 
	/// </summary>
	/// <param name="amount">Amount off money to take.</param>
	public void takeMoney(float amount){
		money -= amount;
		updateUI();
	}

	/// <summary>
	/// 	Adds the amount to the number of lives. 
	/// </summary>
	/// <param name="amount">Amount off lives to add.</param>
	public void addLife(int l = 1){
		lives += l;
		updateUI();
	}

	/// <summary>
	/// 	Takes the amount to the amount of lives. 
	/// </summary>
	/// <param name="amount">Amount off lives to take.</param>
	public void LostLife(int l = 1){
		lives -= l;
		if(lives <= 0){
			gameOver();
		}
		updateUI();
	}

	/// <summary>
	/// 	Loops through the UI elements that need updating and changes them.
	/// </summary>
	void updateUI(){
		foreach(Text t in Money){
			t.text = "Money: " + money;
		}

		foreach(Text t in Lives){
			t.text = "Lives: " + lives;
		}
	}

	/// <summary>
	/// 	Checks if the Player has enough money to purchase something
	/// </summary>
	/// <returns><c>true</c>, if player has enought money , <c>false</c> Doesn't have enought</returns>
	/// <param name="amount">Amount.</param>
	public bool hasEnoughtMoney(float amount){
		if(money >= amount)
			return true;
		return false;
	} 

	void Start(){
		updateUI();
	}

	/// <summary>
	/// Game over.
	/// Reloads currant Level.
	///	TODO change this
	/// </summary>
	void gameOver(){
		Debug.Log("gameOver");

		// Analytics Start
		Tower[] towers = GameObject.FindObjectsOfType<Tower>();
		Enemy[] enemys = GameObject.FindObjectsOfType<Enemy>();

		Analytics.CustomEvent("gameOver", new Dictionary<string, object>
			{
				{ "money", money },
				{ "towers", towers.Length },
				{ "enemys", enemys.Length }
			});
		Debug.Log("Analytics sent to cloud.");
		// Analytics End

		//TODO: Game over scene. Currantly only has one scene
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
