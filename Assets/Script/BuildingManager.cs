﻿using UnityEngine;
using System.Collections;

public class BuildingManager : MonoBehaviour {

	public GameObject TowerPrefab;

	/// <summary>
	/// 	Select the tower that will be used to build in Towerspots.
	/// 	<seealso cref="TowerSport"/>
	/// </summary>
	/// <
	/// <param name="obj">The Prefab that will be used to build</param>

	public void SelectTowerType(GameObject obj){
		TowerPrefab = obj;
	}
}
